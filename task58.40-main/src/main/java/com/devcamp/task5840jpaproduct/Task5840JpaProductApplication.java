package com.devcamp.task5840jpaproduct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task5840JpaProductApplication {

	public static void main(String[] args) {
		SpringApplication.run(Task5840JpaProductApplication.class, args);
	}

}
