package com.devcamp.task5840jpaproduct.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task5840jpaproduct.model.CProduct;

public interface iProductRepository extends JpaRepository<CProduct, Long>{
    
}
