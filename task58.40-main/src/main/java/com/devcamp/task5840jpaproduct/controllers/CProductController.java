package com.devcamp.task5840jpaproduct.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task5840jpaproduct.model.CProduct;
import com.devcamp.task5840jpaproduct.repository.iProductRepository;

@RestController
@CrossOrigin
public class CProductController {
    @Autowired
    iProductRepository productRepository;
    @GetMapping("/products")
    public ResponseEntity<List<CProduct>> getDrinks(){
        try {
            List<CProduct> productList = new ArrayList<CProduct>();
            productRepository.findAll().forEach(productList::add);
            if (productList.size()==0){
                return new ResponseEntity<>(productList,HttpStatus.NOT_FOUND);
            }
            else return new ResponseEntity<>(productList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
