package com.devcamp.task5840jpaproduct.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="products")
public class CProduct {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(name = "ma_san_pham")
    private String maSanPham;
    @Column(name = "ten_san_pham")
    private String tenSanPham;
    @Column(name = "gia_tien")
    private long price;
    @Column(name = "ngay_tao")
    private long ngayTao;
    @Column(name = "ngay_cap_nhat")
    private long ngayCapNhat;
    public CProduct() {
    }
    public CProduct(long id, String maSanPham, String tenSanPham, long price, long ngayTao, long ngayCapNhat) {
        this.id = id;
        this.maSanPham = maSanPham;
        this.tenSanPham = tenSanPham;
        this.price = price;
        this.ngayTao = ngayTao;
        this.ngayCapNhat = ngayCapNhat;
    }
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getMaSanPham() {
        return maSanPham;
    }
    public void setMaSanPham(String maSanPham) {
        this.maSanPham = maSanPham;
    }
    public String getTenSanPham() {
        return tenSanPham;
    }
    public void setTenSanPham(String tenSanPham) {
        this.tenSanPham = tenSanPham;
    }
    public long getPrice() {
        return price;
    }
    public void setPrice(long price) {
        this.price = price;
    }
    public long getNgayTao() {
        return ngayTao;
    }
    public void setNgayTao(long ngayTao) {
        this.ngayTao = ngayTao;
    }
    public long getNgayCapNhat() {
        return ngayCapNhat;
    }
    public void setNgayCapNhat(long ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }
    
}
